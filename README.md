# Enable shared runners in a whole group

GitLab groups allow to [bulk disable shared runners for a group](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#disable-shared-runners-for-a-group) and its subgroups and projects. However, that change can not be rolled back in bulk, only by toggling individual subgroups and projects. That is a time consuming task.

This script does the exact opposite. For a given group, it **enables shared runners for the group, its subgroups and all projects within**.

## Usage

`python3 enable_shared_runners.py $GIT_TOKEN $GROUP_ID`

## Setup and execution

- Import as new project / fork repository.
- In Settings -> CI/CD -> Variables, add a variable "GIT_TOKEN" with a GitLab API token as value. That token needs the scope `api` (read/write access) and the `Owner` role to change the shared runner settings in the sub-groups and the projects.
- Specify the `$GROUP_ID` variable
  - You can specify your group namespace or id in the $GROUP_ID variable in the CI file (by adding `value:` to the variable). The intended use is to specify this variable in the "Run pipeline" interface.
- Run the Pipeline

## Disclaimer

This script is provided for educational purposes. It is **not supported by GitLab**. However, you can create an issue if you need help or propose a Merge Request. This script overwrites previous configuration with no way to roll-back, so use it with caution. Optimally, this is only used if a users accidentally disables shared runners in a group.
