#!/usr/bin/env python3

import gitlab
import argparse
import json
import requests

def get_group_projects(gl, group):
    projects = []
    group_projects = group.projects.list(iterator=True, archived=False, include_subgroups=True, simple=True)
    for group_project in group_projects:
        projects.append(gl.projects.get(group_project.id))
    return projects

def get_subgroups(gl, topgroup, groups):
    subgroups = topgroup.subgroups.list(iterator=True, lazy=True)
    for subgroup in subgroups:
        subgroup = gl.groups.get(subgroup.id)
        groups.append(subgroup)
        get_subgroups(gl, subgroup, groups)
    return groups

parser = argparse.ArgumentParser(description='Turn on all shared runners on all groups and projects of a group')
parser.add_argument('token', help='API token of the group owner')
parser.add_argument('group', help='The group namespace / id to run this report on')
parser.add_argument('--gitlab', help='GitLab instance ', default="https://gitlab.com")

args = parser.parse_args()

gl = gitlab.Gitlab(args.gitlab, private_token=args.token)
gl_api = args.gitlab.strip("/")
gl_api = gl_api + "/api/v4/"
headers = { "PRIVATE-TOKEN": args.token }

try:
    topgroup = gl.groups.get(args.group)
except Exception as e:
    print("[ERROR] can not get group %s" % args.group)
    print(e)
    exit(1)

# it would be nice to use python-gitlab here and display the current setting for each group
# but shared_runners_setting is not available via GET, only via GraphQL, which I don't want to bother with
print("[Info] Resolving group hierarchy")
groups = get_subgroups(gl, topgroup, [topgroup])
for group in groups:
    try:
         print("[Info] Enabling shared runners for group %s" % group.full_path)
         group_api_path = gl_api + "groups/" + str(group.id)
         r = requests.put(group_api_path, data ={'shared_runners_setting':'enabled'}, headers=headers)
         r.raise_for_status()
    except Exception as e:
         print("[Error] Failed to enable shared runners.")
         print(e)

projects = get_group_projects(gl, topgroup)
for project in projects:
    try:
        print("[Info] Enabling shared runners for project %s" % project.path_with_namespace)
        project.shared_runners_enabled = "true"
        project.save()
    except Exception as e:
        print("[Error] Failed to enable shared runners.")
        print(e)

